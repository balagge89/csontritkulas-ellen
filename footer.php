  <!-- 
  START footer
  ======================================== -->
  <footer class="footer" id="contact">
    <div class="container">
        
      <div class="row footer-first" itemscope itemtype="http://schema.org/LocalBusiness">
            <div class="col-sm-6">
              <img class="logo-img svg" id="footer-logo" src="img/pharmacoidea_logo_grey.svg">
              <p><span itemprop="name">Pharmacoidea Fejlesztő és Szolgáltató Kft.</span> | <span itemprop="streetAddress">HU 6726 Szeged, Derkovits fasor 7-11. </span></p>
              <p><a href="mailto:info@pharmacoidea.eu" itemprop="email">info@pharmacoidea.eu</a> | <a href="tel:+36704140703" itemprop="telephone">00 36 70 414 0709</a></p>
            </div>
            <div class="col-sm-3"><p>kövessen minket: <a href="https://www.facebook.com/pharmacoidea/" class="wow zoomIn" data-wow-delay=".25s"><i class="fa fa-facebook"></i></a></p></div>
            <div class="col-sm-3"><p><a id="mentalfitol-link" href="//pharmacoidea.shop.hu" target="_blank">pharmacoidea.shop.hu</a></p></div>
          </div>

      <div class="row">
        <div class="col-sm-3">
          <p class="title">Termékeink indikáció szerint</p>
          <ul class="list-inline nav-menu-footer">
            <li><a href="http://pharmacoidea.shop.hu/shop/9/Idegrendszer" target="_blank">Idegrendszer</a></li>
            <li><a href="http://pharmacoidea.shop.hu/shop/10/Immunrendszer" target="_blank">Immunrendszer</a></li>
            <li><a href="http://pharmacoidea.shop.hu/shop/11/Keringesi" target="_blank">Keringési Rendszer</a></li>
            <li><a href="http://pharmacoidea.shop.hu/shop/12/Anyagcsere" target="_blank">Anyagcsere</a></li>
            <li><a href="http://pharmacoidea.shop.hu/shop/13/Csontok%20%C3%A9s%20%C3%ADz%C3%BCletek" target="_blank">Csontok és Ízületek</a></li>
            <li><a href="http://pharmacoidea.shop.hu/shop/14/Sz%C3%A9ps%C3%A9g,%20Tests%C3%BAly" target="_blank">Szépség, Testsúly</a></li>
            <li><a href="http://pharmacoidea.shop.hu/shop/16/Term%C3%A9kek%20n%C5%91knek" target="_blank">Termékek Nőknek</a></li>
          </ul>
        </div>
        <div class="col-sm-3">
          <p class="title">Termékkategóriák</p>
          <ul class="list-inline nav-menu-footer">
            <li><a href="//www.mentalfitolbolt.hu/shop/8/shake-ek-es-italporok" target="_blank">Gyümölcsporok</a></li>
            <li><a href="//www.mentalfitolbolt.hu/shop/4/orlemenyek" target="_blank">Őrlemények</a></li>
            <li><a href="//www.mentalfitolbolt.hu/shop/7/kapszulak" target="_blank">Étrend-kiegészítők</a></li>
            <li><a href="//www.mentalfitolbolt.hu/shop/3/gabona-termekek" target="_blank">Funkcionális élelmiszerek</a></li>
            <li><a href="//www.mentalfitolbolt.hu/shop/" target="_blank">Ajándéktárgyak</a></li>
          </ul>
        </div>
        <div class="col-sm-3">
          <p class="title">Kutatás-fejlesztés</p>
          <ul class="list-inline nav-menu-footer">
            <li><a href="//www.pharmacoidea.eu/content/9" target="_blank">Génterápia</a></li>
            <li><a href="//www.pharmacoidea.eu/content/11" target="_blank">Gyógyszerfejlesztés</a></li>
            <li><a href="//www.pharmacoidea.eu/content/12" target="_blank">Gyógynövény alapú termékek</a></li>
          </ul>
        </div>
        <div class="col-sm-3">
          <p class="title">Médiamegjelenések</p>
          <ul class="list-inline nav-menu-footer">
            <li><a href="//www.vg.hu/vallalatok/letoha-tamas-az-ev-peldakepe-419906/" target="_blank" rel="nofollow">Világgazdaság</a></li>
            <li><a href="//valasz.hu/magyarokapiacon/falatnyi-egeszseg-66455" target="_blank" rel="nofollow">Heti Válasz</a></li>
            <li><a href="https://magyarnemzet.hu/tudomany-es-technika/2012/07/hippokratesz-a-xxi-szazadban" target="_blank" rel="nofollow">Magyar Nemzet</a></li>
            <li><a href="//hvg.hu/enesacegem/20140124_Letoha_Tamas_interju" target="_blank" rel="nofollow">HVG</a></li>
            <li><a href="//www.u-szeged.hu/sztemagazin/innovacio/harom-fronton-kuzd" target="_blank" rel="nofollow">SZTE Magazin</a></li>
          </ul>
        </div>        
        <div class="col-sm-12 iso-certificates">
          <p><img class="iso-logo" src="img/iso-9001-bw.jpg"></p>
          <ul class="list-inline text-center">
            <li><a href="http://pharmacoidea.com/ISO9001-2015-hu.pdf" target="_blank" class="wow zoomIn" data-wow-delay=".25s">ISO9001-2015 Tanúsítvány (HU)</a></li>
            <li><a href="http://pharmacoidea.com/ISO9001-2015-melleklet-hu.pdf" target="_blank" class="wow zoomIn" data-wow-delay=".35s">ISO9001-2015 Tanúsítvány Melléklet(HU)</a></li>
            <li><a href="http://pharmacoidea.com/ISO9001-2015-en.pdf" target="_blank" class="wow zoomIn" data-wow-delay=".45s">ISO9001-2015 Tanúsítvány (EN)</a></li>
            <li><a href="http://pharmacoidea.com/ISO9001-2015-melleklet-en.pdf" target="_blank" class="wow zoomIn" data-wow-delay=".55s">ISO9001-2015 Tanúsítvány Melléklet (EN)</a></li>
            <li><a href="http://pharmacoidea.com/auditjelentes_pharmacoidea.pdf" target="_blank" class="wow zoomIn" data-wow-delay=".55s">Auditjelentés</a></li>
          </ul>
        </div>
      </div> <!-- //row -->
      <div class="row copyright">
        <div class="col-sm-3">2018 Pharmacoidea Kft. Minden jog fenntartva.</div>
        <div id="impresszum" class="col-sm-3 col-md-offset-6"><a href="#" data-toggle="modal" data-target="#copyright-text">Szerzői Jog</a> | <a href="#" data-toggle="modal" data-target="#impresszum-text">Impresszum</a></div>
      </div>
    </div>  <!-- //container -->
  </footer> <!-- //copyright -->
    <!-- END FOOTER ========================== -->
<!-- Button trigger modal -->
<!-- Impresszum Modal -->
<div class="modal fade" id="impresszum-text" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Impresszum</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <p><b>AZ ÜZEMELTETŐ ADATAI</b></p>
       <p>Az üzemeltető neve: Pharmacoidea Kft.</p>
       <p>Az üzemeltető székhelye: 6726 Szeged, Derkovits fasor 7-11.</p>
       <p>Az üzemeltető postacíme: 6726 Szeged, Derkovits fasor 7-11.</p>
       <p>Az üzemeltető cégbírósági bejegyzési száma: 06-09-010412</p>
       <p>Az üzemeltető adószáma: 13660271-2-06</p>
       <p>Az üzemeltető EU adószáma: HU13660271</p>
       <p>Az üzemeltető bankszámlaszáma: OTP Bank 11735005-20564191-00000000</p>
       <p>IBAN szám: HU31117350052056419100000000</p>
       <p>BIC (SWIFT): OTPV-HU-HB</p>
        <br>
        <p><b>AZ ÜZEMELTETŐ ELÉRHETŐSÉGE</b></p>
        <p>Ügyfélszolgálat: hétfő, kedd, csütörtök: 8-16:30</p>
        <p>szerda: 8-17:30</p>
        <p>péntek: 8-15:30</p>
        <p>Telefon: +36 70 414 0709</p>
        <p>e-mail: info@pharmacoidea.eu</p>
        <br>
        <p><b>AZ ÜZEMELTETŐ WEBOLDALÁNAK TÁRHELYSZOLGÁLTATÓJA</b></p>
        <p>A weboldalak tárhelyszolgáltatója a Magyar Hosting Kft. (székhely: 1132 Budapest, Victor Hugo utca 18-22., telefon: +36 1 700 2323, e-mail: info@mhosting.hu).</p>
      </div>
    </div>
  </div>
</div>

<!-- Copyright Modal -->
<div class="modal fade" id="copyright-text" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Szerzői jog</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <p>Copyright © 2018 Pharmacoidea Kft. Minden jog fenntartva.</p>
      <p>A honlapon megjelent tartalmak másolása, publikálása, vagy egyéb anyagokban történő bárminemű felhasználása kizárólag a tulajdonos előzetes, írásbeli hozzájárulásával lehetséges.</p>
      </div>
    </div>
  </div>
</div>