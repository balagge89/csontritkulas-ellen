<!DOCTYPE html>
<html lang="hu">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="Pharmacoidea Kft.">
  <title>CSONTRITKULÁS ELLEN</title>
	<meta name="description" content="Napi 1 kapszula hozzájárul a csontok normál állapotának fenntartásához, támogatja a változókor után módosult csontszerkezetben az ásványi anyagok pótlását és segít csökkenteni a csontok ásványi anyag veszteségét.">
" />
	<meta name="keywords" content="" />
  <meta http-equiv="Cache-control" content="public">
  <meta http-equiv="max-age" content="86400">

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content="CSONTRITKULÁS ELLEN"/>
	<meta property="og:image" content="http://csontritkulasellen.hu/img/cover.png"/>
	<meta property="og:url" content="http://csontritkulasellen.hu"/>
	<meta property="og:site_name" content="Csontritkulás Ellen étrend-kiegészítő"/>
	<meta property="og:description" content="Napi 1 kapszula hozzájárul a csontok normál állapotának fenntartásához, támogatja a változókor után módosult csontszerkezetben az ásványi anyagok pótlását és segít csökkenteni a csontok ásványi anyag veszteségét.">
"/>
  
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/animate.min.css" rel="stylesheet"> 
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/flexslider.css" rel="stylesheet">
  <link href="css/owl.carousel.css" rel="stylesheet">
  <link href="css/owl.theme.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/responsive.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

 

  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->
  
  <link rel="shortcut icon" href="img/favicon.ico">

 <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-196524674-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-196524674-1');
</script>

  
</head><!--/head-->

<body>


<!-- main-navigation -->
<div class="navbar menu-wrap" itemscope itemtype="http://schema.org/LocalBusiness">
  <div class="navbar-header text-center">
    <a class="navbar-brand logo-right" href="#home" ><img src="img/pharmacoidea_logo_white.svg" alt="logo" itemprop="logo"></a>
  </div>
    <ul id="main-navigation" class="nav navbar-nav main-navigation">
      <li><a href="#supplement-facts">Így működik</a></li>
      <li><a href="#features-2">Jó Tudni</a></li>
      <li><a href="#real-man">Orvosaink</a></li>
      <li><a href="#product-carousel">További Termékeink</a></li>
      <li><a href="#mission-video">Vállalati Misszió</a></li>
      <li><a href="#contact">Kapcsolat</a></li>
    </ul>
    <div class="menu-contact"><i class="fa fa-info-circle contact-info" aria-hidden="true"></i><a id="telefon" class="contact-info" href="tel:+36704140709"><b itemprop="telephone">+36 70 414 0709</b></a> <span class="contact-info"> <br> <a href="mailto:info@pharmacoidea.eu" itemprop="email">info@pharmacoidea.eu</a></span></div>
    <button class="close-button" id="close-button">Close Menu</button>
</div> <!-- //main-navigation -->


<!-- content wrap -->
<div class="content-wrap">
  <!-- 
  START HOME
  ======================================== -->
  <header id="home">
      <!-- navigation -->
      <nav id="main-navbar">
          <div class="info-bar">
              <div class="container">
                  <div class="pull-left"><a class="logo-left" href="index.html"><img id="logo" src="img/pharmacoidea_logo_white.svg"></a></div>
                  <div class="pull-right "><i class="fa fa-info-circle contact-info" aria-hidden="true" itemscope itemtype="http://schema.org/LocalBusiness"></i><a id="telefon" class="contact-info" href="tel:+36704140709"><b itemprop="telephone">+36 70 414 0709</b></a> <span class="contact-info">vevőszolgálat | <a href="mailto:info@pharmacoidea.eu" itemprop="email">info@pharmacoidea.eu</a></span><button  id="open-button-info"><i class="fa fa-bars"></i></button>  </div>
                  
              </div>
            </div>  
        <div class="container navigation">
          <div class="pull-left">
            <p id="logo-text">TERMÉSZETES VÉDELEM AZ EGÉSZSÉGES CSONTOKÉRT :)</p>
          </div>
          <div class="pull-right menu-icon">
            <a id="top-webshop-link" href="http://pharmacoidea.shop.hu/shop_products/202/pharmacoidea-csontritkulas-ellen-kapszula-extra-kiszereles" target="_blank">vásárlás</a>
            <button  id="open-button">
              <i class="fa fa-bars"></i>
            </button>             
          </div>
          <div class="row" id="popup">
          <div class="col-md-2 col-xs-2 col-md-offset-2"><a href="http://pharmacoidea.eu" target="_blank"><i class="fa fa-info-circle" aria-hidden="true"></i><p>Cégünkről<br> bővebben</p></a></div>
          <div class="col-md-2 col-xs-3"><a href="http://mentalfitolbolt.hu" target="_blank"><i class="fa fa-dropbox" aria-hidden="true"></i><p>További<br> termékeink</p></a></div>  
          <div class="col-md-2 col-xs-2"><a href="http://mentalfitolbolt.hu" target="_blank"><i class="fa fa-percent" aria-hidden="true"></i><p>Aktuális<br> akciók</p></a></div>
          <div class="col-md-2 col-xs-2"><a href="http://mentalfitolbolt.hu" target="_blank"><i class="fa fa-truck" aria-hidden="true"></i><p>10.000Ft felett<br> ingyenes szállítás</p></a></div>
          <div class="col-md-2 col-xs-3"><span id="popup-close">bezár<i class="fa fa-times" aria-hidden="true"></i></span></div>
          </div>


        </div> 
      </nav><!-- //navigation --> <!-- //container -->

      <!-- cover -->
      <div class="container">
       <div class="row cover">
          <div class="col-sm-12">
            <div class="row" itemscope itemtype="http://schema.org/Product">
               <div class="col-lg-7 col-md-6 col-sm-4 col-lg-push-5 col-md-push-6 col-sm-push-8 cover-img">
                 <img itemprop="image" id="cover-img" src="img/csontritkulas_termekkep_header.png" class="wow fadeInRight img-responsive" data-wow-delay="1.50s" alt="main-product">
               </div>
               <div class="col-lg-5 col-md-6 col-sm-8 col-lg-pull-7 col-md-pull-6 col-sm-pull-4 cover-text">
                 <div class="title">
                   <h1 itemprop="name" class="wow fadeInUp" data-wow-delay=".25s">Csontritkulás Ellen</h1>
                   <h4 itemprop="description" class="wow fadeInUp" data-wow-delay=".5s">étrend-kiegészítő klinikailag igazolt <br> hatású összetevőkkel</h4>
                 </div>
                </div>
                <div class="col-lg-12" id="more-info">                
                  <p id="more-info-link">Tudjon meg <br> többet</p>
                   <a href="#supplement-facts"><span class="pulse"></span></a> 
                </div>
             </div>
            
                
           </div>
        </div> <!-- //row & cover -->
    </div> <!-- //container -->

  </header>
  <!-- END HOME ========================== -->
  
  <!-- START FACTS  ======================================== -->
  <section id="supplement-facts" class="supplement-facts">
    <div class="page-header">
        <h2><span class="right-span wow fadeInRight" data-wow-delay=".25s">Így működik</span></h2>

    </div> <!-- //page-header -->

    <div class="container">
      <div class="row" role="tabpanel" itemscope itemtype="http://schema.org/Product"> <!-- row -->
        <div class="col-md-5 col-md-offset-1"> <!-- tab menu -->
          <img itemprop="image" class="supplement-img" src="img/csontritkulas_termekleiras_termekkep.jpg">
          </div>

          <div  class="col-md-5 col-md-offset-1 supplement-details"> 
            <h3  itemprop="name">CSONTRITKULÁS ELLEN <br> étrend-kiegészítő</h3>
            <br>
            <p itemprop="description">Napi 1 kapszula hozzájárul a csontok normál állapotának fenntartásához, támogatja a változókor után módosult csontszerkezetben az ásványi anyagok pótlását és segít csökkenteni a csontok ásványi anyag veszteségét. A csontok alacsony ásványianyag-sűrűsége a csontritkulás miatti csonttörés egyik kockázati tényezője.<br>
            Az 50 év feletti nők a csontritkulás elleni jótékony hatást legalább napi 1200 mg kalcium (3 kapszula) bevitelével érik el.</p>
            <br>
            <p><b>Fogyasztási javaslat:</b> 1 -3 kapszula naponta, ajánlott étkezések előtt vagy közben fogyasztani.</p>
            <p>Az ajánlott napi fogyasztási mennyiséget ne lépje túl! A termék fogyasztása nem helyettesíti a kiegyensúlyozott, vegyes étrendet és az egészséges életmódot.</p>
            <br>
            <p><b>Kiszerelés:</b><br>
            90 kapszula/doboz (3 havi adag)<br>
            1186 mg / kapszula<br>
            OÉTI: 26959/2021.<br>
            A termék 100%-ban természetes hatóanyagokat tartalmaz, adalékanyagoktól mentes.</p>
            <br>
            <a href="http://pharmacoidea.shop.hu/shop_products/202/pharmacoidea-csontritkulas-ellen-kapszula-extra-kiszereles" target="_blank"><button class="btn order-button btn-primary">vásárlás</button></a>
          </div>
      </div>
    </div>

    <div class="container" id="ingredients">
      <div class="row" role="tabpanel"> <!-- row -->
        <div class="col-md-5 col-md-offset-1"> <!-- tab menu -->
          <div class="tab-title">
            <h3 class="pull-left">Összetevők</h3>
            <h4 class="pull-right"><span>Adagonként</span> <span class="small">(Kapszulánként)</span></h4>
            <div class="clearfix"></div>
          </div>
          <ul class="nav nav-pills nav-stacked" role="tablist">
            <li role="presentation" id="ingredient-1" class="active ingredient">  <!-- facts tab menu #1 -->
              <a href="#f1" aria-controls="f1" role="tab" data-toggle="tab">
                <span class="pull-left">Kalcium </span><span class="small pull-right">400 mg (NRV* 50%)</span>
              </a>
            </li>
            <li role="presentation" id="ingredient-2" class="ingredient"> <!-- facts tab menu #2 -->
              <a href="#f2" aria-controls="f2" role="tab" data-toggle="tab">
                <span class="pull-left">Orvosi zsálya kivonat </span><span class="small pull-right">60 mg</span>
              </a>
            </li>

            <li role="presentation" id="ingredient-3" class="ingredient"> <!-- facts tab menu #3 -->
              <a href="#f3" aria-controls="f3" role="tab" data-toggle="tab">
                <span class="pull-left">Cink </span><span class="small pull-right">1,5 mg (NRV* 15%)</span>
              </a>
            </li>
            <li role="presentation" id="ingredient-4" class="ingredient"> <!-- facts tab menu #3 -->
              <a href="#f4" aria-controls="f4" role="tab" data-toggle="tab">
                <span class="pull-left">D3-vitamin (kolekalciferol) </span><span class="small pull-right">15 μg (NRV* 300%)</span>
              </a>
            </li>
            <li role="presentation" id="ingredient-5" class="ingredient"> <!-- facts tab menu #3 -->
              <a href="#f5" aria-controls="f5" role="tab" data-toggle="tab">
                <span class="pull-left">K1-vitamin (fillokinon) </span><span class="small pull-right">11,25 μg</span>
              </a>
            </li>
              

          </ul>
       </div><!-- //tab menu -->

        <div class="col-md-4 col-md-offset-1"> <!-- right-content -->
          <!-- Tab panes -->
          <div class="tab-content product-content">

          <div role="tabpanel" class="tab-pane fade in active slideInRight" id="f1"> <!-- facts #1 -->
              <div class="image-part">
                <div class="product-image">
                  <div>
                    <img src="img/osszetevok/kalcium.jpg" alt="Kalcium">
                  </div>
                </div>
              </div>
              <div class="title">
                <h4>Kalcium <span class="small">400 mg (NRV* 50%)</span></h4>
              </div>
              <p>
              A kalcium hozzájárul a normális izomműködéshez, támogatja a normál véralvadást és az idegrendszer normál működését.
              <br>
              <span class="ingredient-chevron">
                <span class="glyphicon glyphicon-chevron-right" onclick="pagination('2', '1')"></span>
              </span>
              </p>        
            </div>

            <div role="tabpanel" class="tab-pane fade in active slideInRight" id="f2"> <!-- facts #1 -->
              <div class="image-part">
                <div class="product-image">
                  <div>
                    <img src="img/osszetevok/zsalya.jpg" alt="Orvosi zsálya kivonat">
                  </div>
                </div>
              </div>
              <div class="title">
                <h4>Orvosi zsálya kivonat <span class="small">60 mg</span></h4>
              </div>
              <p>
              A menopauza okozta hőhullámok és izzadás kivédésében segít.
              <br>
              <span class="ingredient-chevron">
              <span class="glyphicon glyphicon-chevron-left" onclick="pagination('1', '2')"></span>
                <span class="glyphicon glyphicon-chevron-right" onclick="pagination('3', '2')"></span>
              </span>
              </p>        
            </div>

            <div role="tabpanel" class="tab-pane fade in active slideInRight" id="f3"> <!-- facts #1 -->
              <div class="image-part">
                <div class="product-image">
                  <div>
                    <img src="img/osszetevok/cink.jpg" alt="Cink">
                  </div>
                </div>
              </div>
              <div class="title">
                <h4>Cink <span class="small">1,5 mg (NRV* 15%)</span></h4>
              </div>
              <p>
              Immunerősítő ásványi anyag, mely támogatja a normál szénhidrát, zsírsav- és sav-bázis anyagcserét és hozzájárul a sejtek oxidatív stresszel szembeni védelméhez.
              <br>
              <span class="ingredient-chevron">
              <span class="glyphicon glyphicon-chevron-left" onclick="pagination('2', '3')"></span>
                <span class="glyphicon glyphicon-chevron-right" onclick="pagination('4', '3')"></span>
              </span>
              </p>        
            </div>

            <div role="tabpanel" class="tab-pane fade in active slideInRight" id="f4"> <!-- facts #1 -->
              <div class="image-part">
                <div class="product-image">
                  <div>
                    <img src="img/osszetevok/vitaminok.jpg" alt="D3-vitamin (kolekalciferol)">
                  </div>
                </div>
              </div>
              <div class="title">
                <h4>D3-vitamin (kolekalciferol) <span class="small">15 μg (NRV* 300%)</span></h4>
              </div>
              <p>
              A kalcium és foszfor felszívódásában szerepe van, ami jótékony hatású a vér kalcium szintjére. Immunerősítő, sejtosztódásban is részt vesz.
              <br>
              <span class="ingredient-chevron">
              <span class="glyphicon glyphicon-chevron-left" onclick="pagination('3', '4')"></span>
                <span class="glyphicon glyphicon-chevron-right" onclick="pagination('5', '4')"></span>
              </span>
              </p>        
            </div>

            <div role="tabpanel" class="tab-pane fade in active slideInRight" id="f5"> <!-- facts #1 -->
              <div class="image-part">
                <div class="product-image">
                  <div>
                    <img src="img/osszetevok/vitaminok.jpg" alt="K1-vitamin (fillokinon)">
                  </div>
                </div>
              </div>
              <div class="title">
                <h4>K1-vitamin (fillokinon) <span class="small">11,25 μg</span></h4>
              </div>
              <p>
              A csontok egészségét támogató vitamin, mely segít a normál véralvadás fenntartásában is.
              <br>
              <span class="ingredient-chevron">
              <span class="glyphicon glyphicon-chevron-left" onclick="pagination('4', '5')"></span>

              </span>
              </p>        
            </div>


          </div> <!-- //Tab panes -->
        </div><!-- //right-content -->
      </div> <!-- //row -->
    </div> <!-- //container -->
  </section>
  <!-- END FACTS ========================== -->
  
  <!-- 
  START FEATURES
  ======================================== -->
  <section class="features">
    <!-- counter-content -->
    

    <!-- our-features -->
    <div id="features" class="our-features">
      <div class="page-header">
          <h2><span class="right-span wow fadeInRight" data-wow-delay=".25s">Ajánlott</span></h2>
          <div class="container">
              <div class="row">
                <div class="col-md-2 col-md-offset-1 left-part"><img src="img/kapszula_ajanlott.png"></div>
                <div class="col-md-4 col-md-offset-0 center-part">
                  <ul>
                    <li>Hozzájárul a csontok normál állapotának fenntartásához</li>
                    <li>Segít csökkenteni a csontok ásványianyag veszteségét</li>
                    <li>Támogatja a változókor után módosult csontszerkezetben az ásványi anyagok pótlását</li>
                  </ul>
                </div>
                <div class="col-md-3 right-part"><a href="http://pharmacoidea.shop.hu/shop_products/202/pharmacoidea-csontritkulas-ellen-kapszula-extra-kiszereles" target="_blank"><button class="btn order-button btn-primary">vásárlás</button></a></div>
              </div>
          </div>
      </div>
      </div>
      <div id="features-2" class="our-features">
      <div class="page-header">
          <h2><span class="right-span wow fadeInRight" data-wow-delay=".25s">Jó tudni</span></h2>
      </div>
      <div class="container">
        <div class="row">
            <div id="accordion">
            
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                      <button id="jt_1" onmousedown="autoScrollTo('jt_1');" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <h3>Csontritkulás (Oszteóporozis)</h3>
                      </button>
                      <span class="info-icon">&#x1F6C8;</span>
                    </h5>
                  </div>
                 
                  <div id="collapseOne" class="collapse in" aria-labelledby="headingOne" data-parent="#accordion">    
                    <div class="card-body">
                        <img class="placeholder-img" src="img/jt_1.jpg">
                        <p>A csontritkulás gyakori betegség, amelynek következtében a csontok vékonyabbak és hajlamosabbak a törésekre. A törések különböző egészségügyi problémákat okozhatnak, például állandó fájdalmat, görnyedt testtartást vagy mozgási nehézségeket. A törés utáni nem megfelelő rehabilitáció tartós mozgásszervi problémákhoz is vezethet. Sok ember tekintélyes mennyiségű csontot veszít az élete során. Nincsenek tünetek, amelyekből ezt látni lehetne. Szerencsére a csontritkulás megfelelően kezelhető gyógyszerekkel és megfelelő életmóddal. Ha az egészséges életmódot a lehető legkorábban beépíti az életébe, megelőzheti a csontvesztést, és ezáltal csökkentheti a törések lehetőségét. A <a href="http://pharmacoidea.shop.hu/shop_products/202/pharmacoidea-csontritkulas-ellen-kapszula-extra-kiszereles" target="_blank">Pharmacoidea CSONTRITKULÁS ELLEN</a> speciális gyógynövény, ásványi anyag- és vitamin összetételének köszönhetően hozzájárul a csontok normál állapotának fenntartásához, támogatja a változókor után módosult csontszerkezetben az ásványi anyagok pótlását és segít csökkenteni a csontok ásványi anyag veszteségét.</p>
                        </div>
                    </div>
                  </div>
                  
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                      <button id="jt_2" onmousedown="autoScrollTo('jt_2');" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <h3>Csontritkulás kialakulása</h3>
                      </button>
                      <span class="info-icon">&#x1F6C8;</span>
                    </h5>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
                        <img class="placeholder-img" src="img/jt_2.jpg">
                        <p>Bizonyos esetekben a csontritkulás oka egészségügyi problémák vagy más orvosi kezelések. Az étkezési rendellenességek, például az anorexia és a bulimia csontgyengeséget okozhatnak. Az osteoporosis egyéb okai lehetnek: 
                        <ul> 
                        <li>Kezeletlen gluténérzékenység: Ha egy ilyen betegségben szenvedő személy gluténtartalmú ételeket fogyaszt, az károsítja a vékonybélt, ezáltal károsítja az emésztőrendszert és a csontokat.</li>
                        <li>A pajzsmirigy túl aktív: A betegség kezelésére használt gyógyszerek csontritkulást is okozhatnak.</li>
                        <li>Kemoterápia: A sugárzás okozta csontgyengeség szintén szerepet játszhat a csontritkulás kialakulásában. </li>
                        <li>Más gyógyszerek: Gyógyszerek, például szteroidok és görcsoldók. Az étkezési rendellenességek, a megfelelő testmozgás hiánya és az egészségtelen táplálkozás mind hozzájárulnak a csontritkulás kialakulásához. </li>
                          </ul>  
                        </p>
                        </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h5 class="mb-0">
                      <button id="jt_3" onmousedown="autoScrollTo('jt_3');" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          <h3>Csontritkulás tünetei</h3>
                      </button>
                      <span class="info-icon">&#x1F6C8;</span>
                    </h5>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                      <img class="placeholder-img" src="img/jt_3.jpg">
                      <p>A csontritkulás általában nem okoz semmilyen tünetet. Ellenben sok év után megjelenhetnek az első tünetek, mint: hátfájás, magasság csökkenése, görnyedt tartás. Sok beteg esetében az első tünet egy törött csont, leggyakrabban a csípőben vagy a gerincben. Ha a betegség komolyabbá válik, az ülés, állás, köhögés vagy akár az ölelés is fájdalmas csonttörést okozhat. Kezeletlenül az első törés után még valószínűbb, hogy több csont is el fog törni. A csonttöréses gerincfájdalmat könnyen meg lehet különböztetni a gerincsérv vagy a súlyos gerincferdülés miatti fájdalomtól, mert sokkal erősebb és hirtelenebb, máshoz nem hasonlítható fájdalmat tud okozni. Néhány ember esetében a fájdalom enyhül, ahogy a csont gyógyul, mások esetében tartós fájdalom jelentkezik. A tartós fájdalom kialakulásának esélyét megfelelő csonttörés utáni gyógytorna rehabilitációval minimalizálni lehet. Fontos, hogy fenti panaszok észlelése esetén azonnal forduljunk orvoshoz! A <a href="http://pharmacoidea.shop.hu/shop_products/202/pharmacoidea-csontritkulas-ellen-kapszula-extra-kiszereles" target="_blank">Pharmacoidea CSONTRITKULÁS ELLEN</a> terméke a csontok ásványi anyag sűrűségének fokozásával támogatja a csontok egészségének hosszútávú fenntartását, hozzájárulva ezzel a csontritkulás kockázatának csökkentéséhez.
</p>
                      </div>
                  </div>
                </div>

                <div class="card">
                  <div class="card-header" id="headingFour">
                    <h5 class="mb-0">
                      <button id="jt_4" onmousedown="autoScrollTo('jt_4');" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        <h3>Csontritkulás és menopauza kapcsolata</h3>
                      </button>
                      <span class="info-icon">&#x1F6C8;</span>
                    </h5>
                  </div>
                  <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                    <div class="card-body">
                        <img class="placeholder-img" src="img/jt_4.jpg">
                        <p>Menopauzakor egy nagy arányú ösztrogéncsökkenés történik a nőkben. Ez gyorsítja a csontvesztést  és lelassítja a csontátalakítást. Ez a folyamat még körülbelül a menopauza után 10 évig folytatódik. Végül a csontvesztés sebessége csökken és visszatér a menopauza előtti szintre, az új csontok termelődése azonban nem, így csökken a teljes csonttömeg, ezáltal nagyban növelve a csonttörés esélyeit. A korai menopauza (40 éves kor előtt) szintén növeli a csontritkulás, így egy csonttörés kialakulásának esélyeit. Ugyanez igaz a hosszú ideig tartó alacsony hormonszinttel rendelkezőkre, amely azon nőkre jellemző, akik sok, megerőltető edzést végeznek (sportolók, nehéz fizikai munka). A <a href="http://pharmacoidea.shop.hu/shop_products/202/pharmacoidea-csontritkulas-ellen-kapszula-extra-kiszereles" target="_blank">Pharmacoidea CSONTRITKULÁS ELLEN</a> terméke támogatja a változókor után módosult csontszerkezetben az ásványi anyagok pótlását. Az 50 év feletti nők a csontritkulás elleni jótékony hatást legalább napi 1200 mg kalcium (3 kapszula) bevitelével érik el.</p>
                        
                      </div>
                  </div>
                </div>

              </div>
  </section>
  <!-- END FEATURES ========================== -->
    
  <!-- 
  START REAL MAN
  ======================================== -->
  <section id="real-man" class="real-man">
      <div class="page-header">
          <h2><span class="right-span wow fadeInRight" data-wow-delay=".25s">Orvosaink</span></h2>
          <!--<p class="subtitle wow fadeInUp" data-wow-delay=".25s">We’re brave enough to show what we use.</p>-->
      </div> <!-- //page-header -->

    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <!-- slider start -->
          <div class="flexslider" >        
            <!-- Wrapper for slides -->
            <ul id="orvosaink-slider" class="slides">
              <!-- each-item -->
              <li class="each-item">
                <div class="row">
                  <div class="col-sm-4 col-sm-offset-2 col-md-offset-2 left-part">

                    <div class="doctor-description" itemscope itemtype="http://schema.org/Person">
                      <p class="doctor-name" itemprop="employee">Dr. Letoha Tamás</p>
                      <p class="doctor-position">kutatóorvos</p>
                      <br>
                      <p class="doctor-details">A Példakép díjjal jutalmazott Dr. Letoha Tamás a Pharmacoidea élén Európa legnagyobb gyógyszerfejlesztési projektjeiben vesz részt, a jelenleg gyógyíthatatlan betegségek elleni terápiás megoldásokat kutatva, de emellett nagy hangsúlyt helyez a klinikailag igazolt hatású gyógynövény hatóanyagok hasznosítására, mivel azok "a szintetikumokra jellemző mellékhatások nélkül képesek támogatni a szervezet egészséges működését".</p>
                    </div>
                   
                    
                  </div>
                  <img src="img/dr_letoha_tamas.jpg" class="img-responsive doctor-image" alt="real man">

                </div>
              </li> <!-- //each-item -->
              <li class="each-item">
                  <div class="row">
                    <div class="col-sm-4 col-sm-offset-2 col-md-offset-2 left-part">

                      <div class="doctor-description" itemscope itemtype="http://schema.org/Person">
                        <p class="doctor-name" itemprop="employee">Dr. Letoha Annamária</p>
                        <p class="doctor-position">belgyógyász szakorvos</p>
                        <br>
                        <p class="doctor-details">A Szegedi Tudományegyetem szakorvosaként évtizedes gyógyítói múlttal rendelkezik a belgyógyászati kórképek terápiájában. Fő szakterülete a magas vérnyomás és annak szövődményei. A mindennapi gyógyítói gyakorlat során Dr. Letoha Annamária támogatja a klinikailag igazolt hatású gyógynövények felhasználását a betegségek megelőzésére és a gyógyszeres terápia kiegészítésére.</p>
                      </div>
                     
                      
                    </div>
                    <img src="img/dr_letoha_annamaria.jpg" class="img-responsive doctor-image" alt="real man">

                  </div>
                </li> <!-- //each-item -->

            </ul>
            <!-- custom navigation -->
             <div class="custom-navigation">
              <a href="#" class="flex-prev"><img id="orvosaink-prev" src="img/prev.png" alt="previous" onmouseover="onHoverLeft();" onmouseout="offHoverLeft();"></a>
              <div class="custom-controls-container"></div>
              <a href="#" class="flex-next"><img id="orvosaink-next" src="img/next.png" alt="next" onmouseover="onHoverRight();" onmouseout="offHoverRight();"></a>
            </div> <!-- //custom navigation -->
          </div> <!-- //slider end -->
        </div>
      </div> <!-- //row -->
    </div> <!-- //container -->
  </section>
  <!-- END REAL MAN ========================== -->
 
  <!-- START CAROUSEL ========================== -->

  


  <!-- START CAROUSEL ========================== -->

  <section id="product-carousel">
      <div class="page-header">
          <h2><span class="right-span wow fadeInRight" data-wow-delay=".25s">További Termékeink</span></h2>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-xs-11 col-md-10 col-centered">
      
            <div id="carousel" class="carousel slide" data-ride="carousel" data-type="multi" data-interval="2500">
              <div class="carousel-inner">
                
                <div class="item active">
                  
                  <div class="carousel-col">
                    <div class="block img-responsive"><a href="http://www.mentalfitolbolt.hu/shop_products/131/sziv-egeszsege" target="_blank"><img src="img/sziv-egeszsege.png" class="img-responsive"></a></div>
                    <div class="block product-name img-responsive"><p>Szív Egészsége</p></div>
                  </div>
                                
                  <div class="carousel-col">
                    <div class="block img-responsive"><a href="http://www.mentalfitolbolt.hu/shop_products/93/erek-egeszsege" target="_blank"><img src="img/erek-egeszsege.png" class="img-responsive"></a></div>
                    <div class="block product-name img-responsive"><p>Erek Egészsége</p></div>
                  </div>
                              
                  <div class="carousel-col">
                    <div class="block img-responsive"><a href="http://www.mentalfitolbolt.hu/shop_products/92/optimalis-koleszterin" target="_blank"><img src="img/optimalis-koleszterin.png" class="img-responsive"></a></div>
                    <div class="block product-name img-responsive"><p>Optimális Koleszterin</p></div>
                  </div>
                
                  <div class="carousel-col">
                    <div class="block img-responsive"><a href="http://www.mentalfitolbolt.hu/shop_products/85/zsirral-szemben" target="_blank"><img src="img/zsirral-szemben.png" class="img-responsive"></a></div>
                    <div class="block product-name img-responsive"><p>Zsírral Szemben</p></div>
                  </div>
                
                </div>

                <div class="item">
                  
                  <div class="carousel-col">
                    <div class="block img-responsive"><a href="http://www.mentalfitolbolt.hu/shop_products/46/voros-szolomag-komplex" target="_blank"><img src="img/voros-szolomag.png" class="img-responsive"></a></div>
                    <div class="block product-name img-responsive"><p>Vörös Szőlőmag Komplex</p></div>
                  </div>
               
                  <div class="carousel-col">
                    <div class="block img-responsive"><a href="http://www.mentalfitolbolt.hu/shop_products/59/voros-szolomag-komplex" target="_blank"><img src="img/voros-szolomag-porkeverek.png" class="img-responsive"></a></div>
                    <div class="block product-name img-responsive"><p>Vörös Szőlőmag Komplex Porkeverék</p></div>
                  </div>
                
                  <div class="carousel-col">
                    <div class="block img-responsive"><a href="http://www.mentalfitolbolt.hu/shop_products/53/omega-varazs" target="_blank"><img src="img/omega-varazs.png" class="img-responsive"></a></div>
                    <div class="block product-name img-responsive"><p>Omega Varázs</p></div>
                  </div>

                  <div class="carousel-col">
                    <div class="block img-responsive"><a href="http://www.mentalfitolbolt.hu/shop_products/67/agyserkento" target="_blank"><img src="img/agyserkento.png" class="img-responsive"></a></div>
                    <div class="block product-name img-responsive"><p>Agyserkentő</p></div>
                  </div>
                
                </div>


              <div class="item">
                  <div class="carousel-col">
                    <div class="block img-responsive"><a href="http://www.mentalfitolbolt.hu/shop_products/77/szivesen" target="_blank"><img src="img/szivesen.png" class="img-responsive"></a></div>
                    <div class="block product-name img-responsive"><p>Szívesen</p></div>
                  </div>

                  <div class="carousel-col">
                    <div class="block img-responsive"><a href="http://www.mentalfitolbolt.hu/shop_products/86/szabad-gyokok-ellen" target="_blank"><img src="img/szabad-gyokok-ellen.png" class="img-responsive"></a></div>
                    <div class="block product-name img-responsive"><p>Szabad Gyökök Ellen</p></div>
                  </div>

                  <div class="carousel-col">
                    <div class="block img-responsive"><a href="http://www.mentalfitolbolt.hu/shop_products/78/nyugalom" target="_blank"><img src="img/nyugalom.png" class="img-responsive"></a></div>
                    <div class="block product-name img-responsive"><p>Nyugalom</p></div>
                  </div>
    
                  <div class="carousel-col">
                    <div class="block img-responsive"><a href="http://www.mentalfitolbolt.hu/shop_products/129/majvedo-komplex" target="_blank"><img src="img/majvedo-komplex.png" class="img-responsive"></a></div>
                    <div class="block product-name img-responsive"><p>Májvédő Komplex</p></div>
                    </div>
                </div>
                
            </div>

        
      
              <!-- Controls -->
              <div class="left carousel-control">
                <a href="#carousel" role="button" data-slide="prev">
                  <img id="termekek-prev" src="img/prev.png" alt="previous" onmouseover="onHoverLeftTermekek();" onmouseout="offHoverLeftTermekek();">
                  <!--
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                    -->
                </a>
              </div>
              <div class="right carousel-control">
                <a href="#carousel" role="button" data-slide="next">
                    <img id="termekek-next" src="img/next.png" alt="next" onmouseover="onHoverRightTermekek();" onmouseout="offHoverRightTermekek();">

                </a>
              </div>
            </div>
      
          </div>
        </div>
      </div>
    
<!-- Item slider end-->
  </section>
  
  <!-- 
  START SUBSCRIBE-FORM
  ======================================== -->
  <section class="subscribe-form" id="feliratkozom">
    <div class="color-overlay">
     <div class="page-header">
         <p class="subtitle wow fadeInUp" data-wow-delay=".25s"><b>Szeretne értesülni az egészség megőrzésének új lehetőségeiről?</b> Íratkozzon fel hírlevelünkre!</p>
    </div> <!-- //page-header -->

     <div class="container">
       <div class="row">
         <div class="col-md-10 col-md-offset-1 text-center">
           <form id="subscribe-form" class="form">
             <div class="col-sm-8 text-field">
               <input id="mc-name" class="input-notify-after" type="text" placeholder="név" name="NEV">
               <input id="mc-email" class="input-notify-after" type="email" placeholder="e-mail cím" name="EMAIL">
             </div>
             <div class="col-sm-4 submit-button">
               <input type="submit" class="btn btn-primary btn-notify-after" value="Feliratkozom">
             </div>
             <div class="subscribe-label col-sm-12">
              <label for="mc-email"></label>
            </div>
           </form>
         </div>
       </div> <!-- //row -->
     </div> <!-- //container -->
   </div>
  </section> <!-- //subscribe-form -->
  <!-- END SUBSCRIBE-FORM ========================== -->

  <!-- MISSION VIDEO ========================== -->
  <section id="mission-video" class="mission">
  <div class="page-header">
          <h2><span class="right-span wow fadeInRight" data-wow-delay=".25s">Videó Riport</span></h2>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-md-offset-0">
              <div class="video-container">
                <iframe class="embed-responsive-item" width="650" height="434" src="https://www.youtube.com/embed/SLIWbplrtI4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                
              </div>
              <p><img id="video-arnyek" src="img/video_embed_arnyek.png"</p>
          </div>
        </div> <!-- //row -->
      </div> <!-- //container -->
    </section>
    <section id="mission-text" class="mission">
        <div class="page-header">
            <h2><span class="right-span wow fadeInRight" data-wow-delay=".25s">Vállalati Misszió</span></h2>
        </div> <!-- //page-header -->
        
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <p>A civilizációs betegségek terjedése egyre növekvő problémát jelent a fejlett ipari társadalmak számára. Az európa legrangosabb gyógyszerfejlesztési programjában résztvevő Pharmacoidea Kft. a számos hazai és nemzetközi elismerésben részesült <i>(NO-BLE Ideas I. hely, EEN Nagykövet, Év Példaképe díj)</i> kutatóorvos, Dr. Letoha Tamás vezetésével új gyógynövény alapú termékcsaládot is fejleszt, melynek természetes hatóanyagai mellékhatások nélkül szolgálják a szervezet egészséges működését, kifogástalan szellemi és fizikai állapotot biztosítva az egészségtudatos fogyasztók számára.</p>
                <p><a href="http://www.pharmacoidea.eu/" target="_blank">tudjon meg többet rólunk</a></p>
              </div>
              <div class="col-md-12 col-md-offset-0" id="tamogato-logok"><img src="img/logok.png"></div>
          </div> <!-- //row -->
        </div> <!-- //container -->
      </section>
  
  <?php include('footer.php'); ?>

</div> <!-- //content-wrap -->




  <!-- script part -->
  <script type="text/javascript" src="js/queryloader2.min.js"></script>
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>
  <script type="text/javascript" src="js/jquery.countTo.js"></script>
  <script type="text/javascript" src="js/jquery.inview.min.js"></script>
  <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
  <script type="text/javascript" src="js/classie.js"></script>
  <script type="text/javascript" src="js/jquery.nav.js"></script>
  <script type="text/javascript" src="js/owl.carousel.min.js"></script>
  <script type="text/javascript" src="js/jquery.ajaxchimp.min.js"></script>
  <script type="text/javascript" src="js/form-contact.js"></script>
  <script type="text/javascript" src="js/app.js"></script>
  <script type="text/javascript" src="js/smoothscroll.min.js"></script>
  <script type="text/javascript" src="js/custom.js"></script>

</body>
</html>